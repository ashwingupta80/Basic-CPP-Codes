#include<iostream>

using namespace std;

template <class type>

class ud_stack {
    type Stack[100];
    int top;

  public:
    ud_stack () {
      top = 0;
    }
    void push(type var) {
      Stack[top++] = var;
    }
    type pop () {
      return Stack[--top];
    }
};

int main() {

  ud_stack <float> s1;

  s1.push(22.5);
  s1.push(22.6);
  s1.push(22.7);

  cout << s1.pop() << endl;
  cout << s1.pop() << endl;
  cout << s1.pop() << endl;

  return 0;
}
